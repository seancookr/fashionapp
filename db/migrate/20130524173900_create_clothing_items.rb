class CreateClothingItems < ActiveRecord::Migration
  def change
    create_table :clothing_items do |t|
      t.string :clothing_name
      t.string :clothing_brand
      t.string :clothing_type
      t.string :clothing_color1
      t.string :clothing_color2
      t.decimal :clothing_price
      t.string :clothing_url

      t.timestamps
    end
  end
end
