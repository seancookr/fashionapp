class AddAttachmentPhotoToClothingItems < ActiveRecord::Migration
  def self.up
    change_table :clothing_items do |t|
      t.attachment :photo
    end
  end

  def self.down
    drop_attached_file :clothing_items, :photo
  end
end
