class ClothingItem < ActiveRecord::Base
  attr_accessible :clothing_brand, :clothing_color1, :clothing_color2, :clothing_name,
                  :clothing_price, :clothing_type, :clothing_url, :photo
  
  validates   :clothing_name,  presence: true
  validates   :clothing_brand, presence: true
  validates   :clothing_color1, presence: true
  validates   :clothing_price, presence: true
  validates   :clothing_type, presence: true
  validates   :clothing_url, presence: true
                  
  has_attached_file :photo, :styles => { :small => "300x300>" }
  
  def cap_name(clothing_name)
    name_list = clothing_name.split
    name_list.each do |item|
      item.capitalize
    end
    cap_name = name_list.join(' ')
    cap_name
  end
  
end
